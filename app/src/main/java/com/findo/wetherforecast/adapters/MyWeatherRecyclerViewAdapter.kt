package com.findo.wetherforecast.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.findo.wetherforecast.database.entities.CityWeather
import com.findo.wetherforecast.databinding.FragmentHomeBinding

class MyWeatherRecyclerViewAdapter(
    private val clickListener: WeatherAdapterListener
) : ListAdapter<CityWeather, MyWeatherRecyclerViewAdapter.ViewHolder>(CityWeatherDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder private constructor(private val binding: FragmentHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: CityWeather, clickListener: WeatherAdapterListener) {
            binding.item = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = FragmentHomeBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class WeatherAdapterListener(
        val clickListener: (sleepId: Int) -> Unit,
        val deleteListener: (city: CityWeather) -> Unit
    ) {
        fun onClick(weather: CityWeather) = clickListener(weather.id)
        fun onDelete(weather: CityWeather) = deleteListener(weather)
    }

    class CityWeatherDiffCallback : DiffUtil.ItemCallback<CityWeather>() {

        override fun areItemsTheSame(oldItem: CityWeather, newItem: CityWeather): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: CityWeather, newItem: CityWeather): Boolean {
            return oldItem == newItem
        }
    }
}