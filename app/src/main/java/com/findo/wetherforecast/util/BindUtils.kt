package com.findo.wetherforecast.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.findo.wetherforecast.R

@BindingAdapter("weatherIconUrl")
fun bindWeatherIconUrl(imgView: ImageView, icon: String?) {
    icon?.let {
        Glide.with(imgView.context)
            .load("https://openweathermap.org/img/wn/$icon@2x.png")
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.ic_add_circle_24)
                    .error(R.drawable.ic_add_circle_24))
            .into(imgView)
    }
}