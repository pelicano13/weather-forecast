package com.findo.wetherforecast.repository

import com.findo.wetherforecast.database.WeatherForecastDao
import com.findo.wetherforecast.database.entities.CityWeather
import com.findo.wetherforecast.network.OpenWeatherMapApiImpl
import com.findo.wetherforecast.network.entities.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class WeatherForecastRepository @Inject constructor(
    private val database: WeatherForecastDao,
    private val remote: OpenWeatherMapApiImpl
) {
    val cities = database.getCitiesWeather()

    suspend fun newCity(lat: String, lng: String) = withContext(Dispatchers.IO) {
        val weather = remote.getWeather(lat, lng).asDatabaseModel()
        database.insert(weather)
        weather
    }

    suspend fun newCity(name: String) = withContext(Dispatchers.IO) {
        var weather: CityWeather? = null
        try {
            weather = remote.getWeather(name).asDatabaseModel()
            database.insert(weather)
        } catch (e: Exception) {
            Timber.e(e)
        }
        weather
    }

    suspend fun getCity(id: Int) = withContext(Dispatchers.IO) {
        database.getCityWeatherId(id).firstOrNull()
    }

    suspend fun delete(city: CityWeather) {
        database.delete(city)
    }
}