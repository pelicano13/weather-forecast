package com.findo.wetherforecast.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.findo.wetherforecast.MainActivity
import com.findo.wetherforecast.R
import com.findo.wetherforecast.databinding.FragmentDetailBinding
import com.findo.wetherforecast.viewmodels.DetailViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber


@AndroidEntryPoint
class DetailFragment : Fragment(), OnMapReadyCallback {
    private val viewModel: DetailViewModel by viewModels()
    private lateinit var binding: FragmentDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(inflater, container, false)

        val cityWeatherId = DetailFragmentArgs.fromBundle(requireArguments()).cityWeatherId
        viewModel.configure(cityWeatherId)

        try {
            binding.run {
                mapView.onCreate(savedInstanceState)
                mapView.onResume()
                MapsInitializer.initialize(requireActivity().applicationContext)
                mapView.getMapAsync(this@DetailFragment)
            }
        } catch (e: Exception) {
            Timber.e(e)
        }

        return binding.root
    }

    override fun onMapReady(googleMap: GoogleMap) {
        viewModel.cityWeather.observe(viewLifecycleOwner, {
            it?.let {
                val latLng = LatLng(it.lat.toDouble(), it.lon.toDouble())
                googleMap.addMarker(MarkerOptions().position(latLng).title(it.name))
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12F))
                binding.run {
                    temperatureVal.text = getString(R.string.temperatures_placeholder, it.temperature)
                    maxTempVal.text = getString(R.string.temperatures_placeholder, it.tempMax)
                    minTempVal.text = getString(R.string.temperatures_placeholder, it.tempMin)
                    pressureVal.text = it.pressure.toString()
                    humidityVal.text = getString(R.string.humidity_placeholder, it.humidity)
                }
                (activity as MainActivity).supportActionBar?.title = it.name
            }
        })
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding.mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.mapView.onLowMemory()
    }
}