package com.findo.wetherforecast.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.findo.wetherforecast.R
import com.findo.wetherforecast.adapters.MyWeatherRecyclerViewAdapter
import com.findo.wetherforecast.databinding.FragmentHomeListBinding
import com.findo.wetherforecast.viewmodels.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var binding: FragmentHomeListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val adapter =
            MyWeatherRecyclerViewAdapter(MyWeatherRecyclerViewAdapter.WeatherAdapterListener({
                viewModel.onWeatherClicked(it)
            }, {
                viewModel.onDelete(it)
            }))

        binding = FragmentHomeListBinding.inflate(inflater, container, false)
        binding.list.adapter = adapter
        binding.lifecycleOwner = this

        viewModel.cities.observe(viewLifecycleOwner, {
            it?.let {
                adapter.submitList(it)
            }
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textField.setEndIconOnClickListener {
            binding.textField.editText?.run {
                val city = text.toString()
                setText("")
                viewModel.newCity(city)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.hasError.observe(viewLifecycleOwner, {
            it?.let {
                Toast.makeText(context, getString(R.string.city_not_found), Toast.LENGTH_SHORT)
                    .show()
                viewModel.onHasErrorComplete()
            }
        })

        viewModel.navigateToDetail.observe(viewLifecycleOwner, {
            it?.let {
                findNavController().navigate(
                    HomeFragmentDirections.actionHomeFragmentToDetailFragment(it)
                )
                viewModel.onNavigateComplete()
            }
        })
    }
}
