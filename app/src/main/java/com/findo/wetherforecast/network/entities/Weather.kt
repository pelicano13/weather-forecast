package com.findo.wetherforecast.network.entities

import com.squareup.moshi.Json

data class Weather (
    val id: Int?,
    val name: String?,
    val cod: Int,
    val message: String?,
    val sys: WeatherSys?,
    val main: Statistic?,
    val weather: List<WeatherMetadata>?,
    val coord: Coord?
)

data class WeatherSys (
    val message: Float?,
    val country: String,
    val sunrise: Float,
    val sunset: Float
)

data class Statistic (
    @Json(name = "temp")
    val temperature: Float,
    @Json(name = "temp_min")
    val tempMin: Float,
    @Json(name = "temp_max")
    val tempMax: Float,
    val pressure: Float,
    val humidity: Float
)

data class WeatherMetadata (
    val main: String,
    val description: String,
    val icon: String
)

data class Coord (
    val lon: Float,
    val lat: Float
)