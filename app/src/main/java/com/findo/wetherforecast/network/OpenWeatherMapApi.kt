package com.findo.wetherforecast.network

import com.findo.wetherforecast.BuildConfig
import com.findo.wetherforecast.network.entities.Weather
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject

interface OpenWeatherMapApiService {
    @GET("weather")
    suspend fun getWeather(
        @Query("lat") lat: String,
        @Query("lon") lng: String,
        @Query("appid") appId: String = BuildConfig.APPI_KEY
    ): Weather

    @GET("weather")
    suspend fun getWeather(
        @Query("q") name: String,
        @Query("appid") appId: String = BuildConfig.APPI_KEY
    ): Weather
}

interface OpenWeatherMapApiHelper {
    suspend fun getWeather(lat: String, lng: String): Weather
    suspend fun getWeather(name: String): Weather
}

class OpenWeatherMapApiImpl @Inject constructor(private val service: OpenWeatherMapApiService) : OpenWeatherMapApiHelper {
    override suspend fun getWeather(lat: String, lng: String): Weather = service.getWeather(lat, lng)
    override suspend fun getWeather(name: String): Weather = service.getWeather(name)
}