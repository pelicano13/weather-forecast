package com.findo.wetherforecast.network.entities

import com.findo.wetherforecast.database.entities.CityWeather

fun Weather.asDatabaseModel() : CityWeather = CityWeather(
    id = id?:-1,
    name =  "$name, ${sys?.country}",
    temperature = main?.temperature?:0F,
    pressure = main?.pressure?:0F,
    humidity = main?.humidity?:0F,
    tempMax =main?.tempMax?:0F,
    tempMin =main?.tempMin?:0F,
    icon = weather?.firstOrNull()?.icon?:"",
    lat = coord?.lat?:0F,
    lon = coord?.lon?:0F,
    description = weather?.firstOrNull()?.description?:"",
)