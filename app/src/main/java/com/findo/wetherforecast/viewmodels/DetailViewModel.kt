package com.findo.wetherforecast.viewmodels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.findo.wetherforecast.database.entities.CityWeather
import com.findo.wetherforecast.repository.WeatherForecastRepository
import kotlinx.coroutines.launch

class DetailViewModel @ViewModelInject constructor(private val repository: WeatherForecastRepository) :
    ViewModel() {

    private val _cityWeather = MutableLiveData<CityWeather?>(null)
    val cityWeather: LiveData<CityWeather?>
        get() = _cityWeather

    fun configure(cityWeatherId: Int) {
        viewModelScope.launch {
            _cityWeather.value = repository.getCity(cityWeatherId)
        }
    }
}