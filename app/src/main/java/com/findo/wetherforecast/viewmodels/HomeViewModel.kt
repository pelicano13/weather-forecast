package com.findo.wetherforecast.viewmodels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.findo.wetherforecast.database.entities.CityWeather
import com.findo.wetherforecast.repository.WeatherForecastRepository
import kotlinx.coroutines.launch
import timber.log.Timber

class HomeViewModel @ViewModelInject constructor(private val repository: WeatherForecastRepository) :
    ViewModel() {

    private val _hasError = MutableLiveData<Boolean?>(null)
    val hasError: LiveData<Boolean?>
        get() = _hasError

    private val _navigateToDetail = MutableLiveData<Int?>(null)
    val navigateToDetail: LiveData<Int?>
        get() = _navigateToDetail

    val cities = repository.cities

    fun newCity(lat: String, lng: String) {
        viewModelScope.launch {
            val res = repository.newCity(lat, lng)
            Timber.d("City: ${res.name}")
        }
    }

    fun newCity(name: String) {
        viewModelScope.launch {
            val res = repository.newCity(name)
            if (res == null) {
                _hasError.value = true
            } else
                Timber.d("City: ${res.name}")
        }
    }

    fun onWeatherClicked(weatherId: Int) {
        _navigateToDetail.value = weatherId
        Timber.d("$weatherId")
    }

    fun onNavigateComplete() {
        _navigateToDetail.value = null
    }

    fun onHasErrorComplete() {
        _hasError.value = null
    }

    fun onDelete(city: CityWeather) {
        viewModelScope.launch {
            repository.delete(city)
        }
    }
}