package com.findo.wetherforecast.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CityWeather(
    @PrimaryKey
    val id: Int,
    val name: String,
    val temperature: Float,
    val pressure: Float,
    val humidity: Float,
    val tempMax: Float,
    val tempMin: Float,
    val icon: String,
    val description: String,
    val lat: Float,
    val lon: Float
) {
    val itemId: String get() = id.toString()
}