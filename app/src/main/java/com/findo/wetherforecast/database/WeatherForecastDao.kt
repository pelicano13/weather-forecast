package com.findo.wetherforecast.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.findo.wetherforecast.database.entities.CityWeather

@Dao
interface  WeatherForecastDao {
    @Query("select * from CityWeather")
    fun getCitiesWeather(): LiveData<List<CityWeather>>

    @Query("select * from CityWeather where id=:cityId")
    suspend fun getCityWeatherId(cityId: Int): List<CityWeather>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(city: List<CityWeather>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(city: CityWeather)

    @Delete
    suspend fun delete(city: CityWeather)
}