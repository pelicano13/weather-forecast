package com.findo.wetherforecast.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.findo.wetherforecast.database.entities.CityWeather

@Database(entities = [CityWeather::class], version = 4, exportSchema = false)
abstract class WeatherForecastDatabase : RoomDatabase() {
    abstract val weatherForecast: WeatherForecastDao
}

private lateinit var INSTANCE: WeatherForecastDatabase

fun getDatabase(context: Context): WeatherForecastDatabase {
    synchronized(WeatherForecastDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                context.applicationContext,
                WeatherForecastDatabase::class.java,
                "videos"
            ).fallbackToDestructiveMigration().build()
        }
    }
    return INSTANCE
}