package com.findo.wetherforecast.modules

import android.content.Context
import com.findo.wetherforecast.database.WeatherForecastDao
import com.findo.wetherforecast.database.WeatherForecastDatabase
import com.findo.wetherforecast.database.getDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): WeatherForecastDatabase {
        return getDatabase(appContext)
    }

    @Provides
    fun provideWeatherForecastDao(database: WeatherForecastDatabase): WeatherForecastDao {
        return database.weatherForecast
    }
}